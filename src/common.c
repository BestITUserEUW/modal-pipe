/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 ******************************************************************************/

#include <stdio.h> // for fprintf
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h> // for pid_t

#include <modal_pipe_common.h>
#include "misc.h"

void pipe_print_error(int e)
{
    if (e == PIPE_ERROR_SERVER_NOT_AVAILABLE)
    {
        fprintf(stderr, "Server not available\n");
    }
    else if (e == PIPE_ERROR_REACHED_MAX_NAME_INDEX)
    {
        fprintf(stderr, "ERROR: Reached maximum number of clients with the same name\n");
    }
    else if (e == PIPE_ERROR_FILE_IO)
    {
        fprintf(stderr, "ERROR: File IO\n");
    }
    else if (e == PIPE_ERROR_TIMEOUT)
    {
        fprintf(stderr, "ERROR: timeout waiting for server\n");
    }
    else if (e == PIPE_ERROR_OTHER)
    {
        fprintf(stderr, "ERROR: other\n");
    }
    else if (e == PIPE_ERROR_INVALID_ARG)
    {
        fprintf(stderr, "ERROR: Invalid Argument\n");
    }
    else if (e == PIPE_ERROR_NOT_CONNECTED)
    {
        fprintf(stderr, "ERROR: not connected\n");
    }
    else if (e == PIPE_ERROR_CTRL_NOT_AVAILABLE)
    {
        fprintf(stderr, "ERROR: control pipe not available\n");
    }
    else if (e == PIPE_ERROR_INFO_NOT_AVAILABLE)
    {
        fprintf(stderr, "ERROR: info pipe not available\n");
    }
    else if (e == PIPE_ERROR_CHANNEL_OOB)
    {
        fprintf(stderr, "ERROR: channel out of bounds\n");
    }
    else if (e < 0)
    {
        fprintf(stderr, "ERROR: unknown error\n");
    }
    // note, there is no final else here so that the client can call this
    // function even when there is no error (e>=0) and nothing will print out.
    return;
}

int pipe_exists(const char *name_or_location)
{
    // construct the full path to where the request FIFO should be
    char request_path[MODAL_PIPE_MAX_PATH_LEN];
    if (pipe_expand_location_string(name_or_location, request_path))
    {
        fprintf(stderr, "ERROR in %s invalid name_or_location\n", __FUNCTION__);
        return 0;
    }
    strcat(request_path, "request");
    // if request fifo exists, we assume the pipe exists
    return _exists(request_path);
}

int pipe_expand_location_string(const char *in, char *out)
{
    // sanity checks
    if (in == NULL || out == NULL)
    {
        fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
        return PIPE_ERROR_INVALID_ARG;
    }

    int len = strlen(in);

    // TODO test for more edge cases
    if (len < 1)
    {
        fprintf(stderr, "ERROR in %s, recevied empty string\n", __FUNCTION__);
        return PIPE_ERROR_INVALID_ARG;
    }
    if (len == 1 && in[0] == '/')
    {
        fprintf(stderr, "ERROR in %s, pipe path can't just be root '/'\n", __FUNCTION__);
        return PIPE_ERROR_INVALID_ARG;
    }

    len = 0;
    // if user didn't provide a full path starting with '/' then prepend the
    // default path and write out, recording the new length.
    if (in[0] != '/')
    {
        len += sprintf(out, "%s", MODAL_PIPE_DEFAULT_BASE_DIR);
    }
    // write out the full path provided (excluding non-printable characters)
    for (int i = 0; in[i] != 0; i++)
    {
        if (in[i] > ' ' && in[i] <= '~')
            out[len++] = in[i];
    }
    out[len] = 0;

    // make sure the path ends in '/' since it should be a directory
    if (out[len - 1] != '/')
    {
        out[len] = '/';
        out[len + 1] = 0;
    }

    return 0;
}
