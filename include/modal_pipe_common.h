/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 ******************************************************************************/

#ifndef MODAL_PIPE_COMMON_H
#define MODAL_PIPE_COMMON_H

#define MODAL_PIPE_DEFAULT_BASE_DIR "/run/mpa/"
#define MODAL_PIPE_MAX_DIR_LEN 64

// Sensible limits of the length of directories and paths
#define MODAL_PIPE_MAX_NAME_LEN 32
#define MODAL_PIPE_MAX_PATH_LEN (MODAL_PIPE_MAX_DIR_LEN + MODAL_PIPE_MAX_NAME_LEN)
#define MODAL_PIPE_MAX_TYPE_LEN 32

#define MODAL_PIPE_DEFAULT_PIPE_SIZE (1024 * 1024) // 1Mb

/**
 * information describing a pipe. Servers use this to create a new pipe in the
 * file system. The data is available in json format in the info file, e.g.
 * /run/mpa/imu0/info for the client to read back along with any other json data
 * the server elects to put in there such as camera lens calibration.
 */
typedef struct pipe_info_t
{
    char name[MODAL_PIPE_MAX_NAME_LEN];        ///< short name, e.g. "imu0"
    char location[MODAL_PIPE_MAX_DIR_LEN];     ///< this is the full pipe location, e.g. /run/mpa/imu0/
    char type[MODAL_PIPE_MAX_TYPE_LEN];        ///< examples: imu_data_t, mavlink_message_t, camera, point cloud
    char server_name[MODAL_PIPE_MAX_NAME_LEN]; ///< name of the server that created the pipe, e.g. voxl-imu-server
    int size_bytes;                            ///< FIFO size that server will create, when client subscribes, can be updated by client
    int server_pid;                            ///< process ID of the server that created the pipe
} pipe_info_t;

#define PIPE_INFO_INITIALIZER                       \
    {                                               \
        .name = "unknown",                          \
        .location = "unknown",                      \
        .type = "unknown",                          \
        .server_name = "unknown",                   \
        .size_bytes = MODAL_PIPE_DEFAULT_PIPE_SIZE, \
        .server_pid = 0                             \
    }

// error codes used throughout the library
// mostly these are used by pipe_client_open()
#define PIPE_ERROR_OTHER -1
#define PIPE_ERROR_SERVER_NOT_AVAILABLE -2
#define PIPE_ERROR_REACHED_MAX_NAME_INDEX -3
#define PIPE_ERROR_FILE_IO -4
#define PIPE_ERROR_TIMEOUT -5
#define PIPE_ERROR_INVALID_ARG -6
#define PIPE_ERROR_NOT_CONNECTED -7
#define PIPE_ERROR_CTRL_NOT_AVAILABLE -8
#define PIPE_ERROR_INFO_NOT_AVAILABLE -9
#define PIPE_ERROR_CHANNEL_OOB -10

/**
 * @brief      print a human readable pipe error number
 *
 *             usually this is to check the return value of pipe_client_open()
 *
 *             pipe_client_init_channel fails silently so that clients can keep
 *             trying to init waiting for a server to come online or for a
 *             channel to free up without cluttering up the screen with error
 *             messages. If the user wants to print which error occured anyway
 *             then they can use this function. See the example
 *             modal-hello-client
 *
 * @param[in]  e     error returned from pipe_client_init_channel
 */
void pipe_print_error(int e);

/**
 * @brief      Check if a pipe name or full location exists and can be opened.
 *
 *             This function can take either a pipe name (e.g. "imu0") or a
 *             complete location of a pipe (e.g. /run/mpa/imu0). If a pipe name
 *             is given, it will be assumed to live in /run/mpa/
 *
 *             Under the hood, this looks to see if the pipe location contains a
 *             request FIFO indicating the server that created the pipe is
 *             active and can receive requests to open the pipe by a client.
 *
 * @param      name_or_location  The name or location
 *
 * @return     1 if pipe exists, 0 otherwise.
 */
int pipe_exists(const char *name_or_location);

/**
 * @brief      Take a pipe name, partial location path, or complete location
 *             path, and write out the full and correct pipe location path
 *             string to the topic directory.
 *
 *             This is not probably not necessary for users to use since
 *             pipe_client_open_pipe() uses this under the hood. You can give
 *             "imu0" to pipe_client_open_pipe and it will automatically expand
 *             the pipe name to the full pipe location /run/mpa/imu0/. However,
 *             if you want to do this expansion for yourself, this function is
 *             made available.
 *
 *             Examples of input > output behavior
 *
 *             - imu0     > /run/mpa/imu0/
 *             - imu0/    > /run/mpa/imu0/
 *             - /foo/bar > /foo/bar/
 *             - /foo     > /foo/
 *
 *             This does not guarantee the path exists, it only formats the
 *             string.
 *
 *             The output pointer should point to a memory location that can
 *             accomodate a string of length MODAL_PIPE_MAX_DIR_LEN
 *
 * @param      in    input string
 * @param      out   pointer to pre-allocated memory to write the result to
 *
 * @return     0 on success, -1 on failure
 */
int pipe_expand_location_string(const char *in, char *out);

#endif // MODAL_PIPE_COMMON_H
